from .models import Empleado, Departamento
from rest_framework import serializers


# class EmpleadoSerializer(serializers.HyperlinkedModelSerializer):
#     class Meta:
#         model = Empleado
#         fields = '__all__'

# class DepartamentoSerializer(serializers.HyperlinkedModelSerializer):
#     class Meta:
#         model = Departamento
#         fields = '__all__'

class EmpleadoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Empleado
        fields = '__all__'