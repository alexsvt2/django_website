from django.shortcuts import render
from apps.recursos_humanos.models import Departamento, Empleado
from django.views.generic import ListView
from .forms import DepartamentoForm, EmpleadoForm
from django.shortcuts import redirect

from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.response import Response


# from apps.recursos_humanos.   serializers import EmpleadoSerializer, DepartamentoSerializer
from apps.recursos_humanos.serializers import EmpleadoSerializer
from django.contrib.auth.decorators import login_required


# class EmpleadoViewSet(viewsets.ModelViewSet):
#     queryset = Empleado.objects.all()
#     serializer_class = EmpleadoSerializer

# class DepartamentoViewSet(viewsets.ModelViewSet):
#     queryset = Departamento.objects.all()
#     serializer_class = DepartamentoSerializer

class Empleado_list(APIView):
    def get(self, request):
        empleados = Empleado.objects.all()
        serialized = EmpleadoSerializer(empleados, many=True)
        return Response(serialized.data)


@login_required()
def recursos_humanos(request):
    return render(request, 'recursos_humanos/recursos_humanos.html')


@login_required()
def recursos_humanos_empleados(request):
    empleados = Empleado.objects.filter(activo=True)
    return render(request, 'recursos_humanos/recursos_humanos_empleados.html', {'empleados': empleados})


@login_required()
def recursos_humanos_empleados_baja(request):
    empleados = Empleado.objects.filter(activo=False)
    return render(request, 'recursos_humanos/recursos_humanos_empleados_baja.html', {'empleados': empleados})


@login_required()
def empleado_new(request):
    if request.method == "POST":
        form = EmpleadoForm(request.POST, request.FILES)
        if form.is_valid():
            empleado = form.save(commit=False)
            empleado.save()
            # return redirect('recursos_humanos_empleados.html', pk=empleado.pk)
            return redirect('recursos_humanos_empleados')
    else:
        form = EmpleadoForm
    return render(request, 'recursos_humanos/recursos_humanos_empleados_new.html', {'form': form})


@login_required()
def departamento_new(request):
    if request.method == "POST":
        form = DepartamentoForm(request.POST)
        if form.is_valid():
            departamento = form.save(commit=False)
            departamento.save()
            return redirect('recursos_humanos_departamentos')
    else:
        form = DepartamentoForm()
    return render(request, 'recursos_humanos/recursos_humanos_departamentos_new.html', {'form': form})

    
@login_required()
def empleado_edit(request, pk):
    # empleado = get_object_or_404(Empleado, pk=pk)
    empleado = Empleado.objects.get(pk=pk)
    if request.method == "POST":
        archivos = request.FILES
        form = EmpleadoForm(request.POST, archivos, instance=empleado)
        if form.is_valid():
            empleado = form.save(commit=False)
            empleado.save()
            return redirect('recursos_humanos_empleados')
    else:
        form = EmpleadoForm(instance=empleado)
    return render(request, 'recursos_humanos/recursos_humanos_empleados_edit.html', {'form': form})


@login_required()
def departamento_edit(request, pk):
    departamento = Departamento.objects.get(pk=pk)
    if request.method == "POST":
        form = DepartamentoForm(request.POST, request.FILES, instance=departamento)
        if form.is_valid():
            departamento = form.save(commit=False)
            departamento.save()
            return redirect('recursos_humanos_departamentos')
    else:
        form = DepartamentoForm(instance=departamento)
    return render(request, 'recursos_humanos/recursos_humanos_departamentos_edit.html', {'form': form})


@login_required()
def empleado_delete(request, pk):
    empleado = Empleado.objects.get(pk=pk)
    empleado.activo = not empleado.activo
    empleado.save()
    # empleado.delete()
    return redirect('recursos_humanos_empleados')


@login_required()
def departamento_delete(request, pk):
    departamento = Departamento.objects.get(pk=pk)
    departamento.activo = not departamento.activo
    departamento.delete()
    return redirect('recursos_humanos_departamentos')


@login_required()
def recursos_humanos_departamentos(request):
    departamentos = Departamento.objects.filter(activo=True)
    return render(request, 'recursos_humanos/recursos_humanos_departamentos.html', {'departamentos': departamentos})
