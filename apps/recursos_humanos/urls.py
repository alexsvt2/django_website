from django.urls import path
from django.views.generic import TemplateView
from apps.recursos_humanos import views as recursos_humanos_views

urlpatterns = [
    path('', recursos_humanos_views.recursos_humanos, name="recursos_humanos"),
    path('empleados_api/', recursos_humanos_views.Empleado_list.as_view()),
    path('empleado/new', recursos_humanos_views.empleado_new, name="empleado_new"),
    path('empleado/delete/<int:pk>', recursos_humanos_views.empleado_delete, name="empleado_delete"),
    path('empleado/delete/', recursos_humanos_views.empleado_delete, name="empleado_delete"),
    path('empleado/edit/<int:pk>', recursos_humanos_views.empleado_edit, name="empleado_edit"),
    path('empleado/edit/', recursos_humanos_views.empleado_edit, name="empleado_edit"),
    path('departamento/edit/<int:pk>', recursos_humanos_views.departamento_edit, name="departamento_edit"),
    path('departamento/new', recursos_humanos_views.departamento_new, name="departamento_new"),
    path('empleado/', recursos_humanos_views.recursos_humanos_empleados, name="recursos_humanos_empleados"),
    path('empleado/baja', recursos_humanos_views.recursos_humanos_empleados_baja, name="recursos_humanos_empleados_baja"),
    path('departamento/', recursos_humanos_views.recursos_humanos_departamentos, name="recursos_humanos_departamentos"),



]

