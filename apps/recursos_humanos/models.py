from django.db import models


class Departamento(models.Model):
    nombre_departamento = models.CharField(
        max_length=200, help_text="Por favor escribe el Nombre del Departamento")
    descripcion_departamento = models.TextField()
    activo = models.BooleanField(
        default=True, verbose_name="Departamento Activo")
    created = models.DateTimeField(
        auto_now_add=True, verbose_name="Fecha de creación")
    updated = models.DateTimeField(
        auto_now=True, verbose_name="Fecha de edición")

    def __str__(self):
        return self.nombre_departamento




class Empleado(models.Model):
    nombre = models.CharField(
        max_length=200, help_text="Escribe tu nombre de Pila")
    apellido = models.CharField(
        max_length=200, help_text="Escribe unicamente tu primer apellido")
    nacimiento = models.DateField()
    salario_mensual = models.DecimalField(
        max_digits=10, decimal_places=2, help_text="Escribe el Salario Mensual")
    direccion = models.CharField(max_length=200, verbose_name="Direccion")
    departamento = models.ForeignKey(Departamento, on_delete=models.CASCADE)
    activo = models.BooleanField(default=True, verbose_name="Usuario Activo")
    esc_primaria = 'Primaria'
    esc_secundaria = 'Secundaria'
    esc_preparatoria = 'Preparatoria'
    esc_licenciatura = 'Licenciatura Terminada'
    esc_licenciatura_trunca = 'Licenciatura Trunca'
    esc_opciones = (
        (esc_primaria, 'Primaria'),
        (esc_secundaria, 'Secundaria'),
        (esc_preparatoria, 'Preparatoria'),
        (esc_licenciatura, 'Licenciatura Trunca',),
        (esc_licenciatura_trunca, 'Licenciatura terminada'),
    )
    escolaridad = models.CharField(
        max_length=40,
        choices=esc_opciones,
    )
    imagen_perfil = models.ImageField(upload_to='images/', default='images/default.jpeg')
# De Momento no esta funcionando subir imagenes directamente desde el formulario, por eso siempre sube la imagen default




    created = models.DateTimeField(
        auto_now_add=True, verbose_name="Fecha de creación")
    updated = models.DateTimeField(
        auto_now=True, verbose_name="Fecha de edición")

    def __str__(self):
        return 'id: %s. %s, %s' % (self.id, self.nombre, self.apellido, )
        # return '%s %s. En Departamento:  %s' % (self.nombre, self.nacimiento, self.departamento)
