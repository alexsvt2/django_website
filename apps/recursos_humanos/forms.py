from django import forms
from .models import Empleado, Departamento


class EmpleadoForm(forms.ModelForm):

    class Meta:
        model = Empleado
        template = 'recursos_humanos/recursos_humanos_empleados_new.html'
        fields = ('nombre', 'apellido', 'salario_mensual',
                  'direccion', 'departamento', 'escolaridad', 'nacimiento','imagen_perfil')
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Nombre'}),
            'apellido': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Apellido'}),
            'nacimiento': forms.TextInput(attrs={'class': 'form-control', 'type': 'text', 'autocomplete': 'off'}),
            'salario_mensual': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Salario Mensual'}),
            'direccion': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Direccion'}),
            'departamento': forms.Select(attrs={'class': 'form-control'}),
            'escolaridad': forms.Select(attrs={'class': 'form-control'}),
        }

        help_texts = {
            'nombre': None,
            'apellido': None,
            'nacimiento': None,
            'salario_mensual': None,
            'direccion': None,
            'departamento': None,
            'activo': None,
            'escolaridad': None,
        }


class DepartamentoForm(forms.ModelForm):
    class Meta:
        model = Departamento
        template_name = 'recursos_humanos/recursos_humanos_departamentos_new.html'
        fields = '__all__'
        help_texts = {
            'nombre_departamento': None,
            'descripcion_departamento': None,
        }

        widgets = {
            'nombre_departamento': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Ejemplo: Contabilidad'}),
            'descripcion_departamento': forms.Textarea(attrs={'class': 'form-control', 'rows': '4'})
        }
