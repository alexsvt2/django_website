var MyFunction;
MyFunction = function() {
    var init, cargar_tabla,global_config

    // En la parte de arriba se crean varias variables con el mismo var
    // var init;
    // var cargar_tabla;
    // var global_config;
    init = function(config) {
        global_config = config
        cargar_tabla();
    };
    var callback_datatable = function(nRow, aData, IdisplayIndex) {
        var edit = $('<a>').attr({
            'href': global_config.url_recursos_humanos_edit_empleado + aData.id,
        }).addClass('btn btn-success').text('Editar');

        var delete_record = $('<button>').click(function() {
            borrar_registro(aData.id)
        }).addClass('btn btn-danger').text('Eliminar');

        var td = $('<td>');
        td.append(edit, delete_record);
        $(nRow).append(td);
    };
    cargar_tabla = function() {
        console.log('hola')
        $.ajax({
                url: global_config.url,
            })
            .done(function(reponse) {
                console.log(reponse)
                $('#DataTable_Recursos_Humanos').DataTable({
                    data: reponse,
                    "fnRowCallback": callback_datatable,
                    "columns": [
                        { "data": "nombre" },
                        { "data": "apellido" },
                        { "data": "direccion" },
                        { "data": "nacimiento" },
                        { "data": "salario_mensual" },
                    ]
                })
            })
    }
    borrar_registro = function(id) {
    	$.ajax({

    	})
        console.log(id)
    }
    return {
        init: init,
    }
}