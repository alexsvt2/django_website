# Django_WebSite

Para Levantar el Proyecto, se debe configurar la base de datos


```
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'myproject_db',
        'USER': 'myproject_admin',
        'PASSWORD': '..',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}
```

Se deben instalar las librerias/dependencias con pip3 en un entorno virtual (virtualenv)
pip3 install -r requiriements.txt


Se recomienda instalar virtualenv

Recomiendo correr
virtualenv -p python3.7 venv 

(venv) representa el nombre del entorno virtual y siempre debe ir un directorio arriba de la ubicacion del proyecto, nunca dentro del entorno
```
.
├── django_website
│   ├── apps
│   ├── manage.py
│   ├── media
│   ├── myproject
│   ├── README.md
│   ├── requiriments.txt
│   ├── static
│   └── templates
└── venv
    ├── bin=
```

El proyecto corre sobre Django 2.1.7, Python 3.7.2 Postgresql 10.6


___ESTA DOCUMENTACION DE MOMENTO FUNCIONA EN FORMA DE APUNTES AL DESARROLLADOR___

Lista de Cosas por Practicar

Cheklist Basico Python/Django CRUD (ejemplo registro de alumnos, personal, etc...)
-[ ] Aplicación crud usando python y django en sus ultimas versiones
-[ ] Integrar a la aplicación crud una base de datos postgresql usando la libreria mas estable y actualizada para ello
-[ ] En el formulario del crud, que se usaria tanto para editar como para crear registros, implementar la mayor cantidad posible de diferentes valores para la practica de guardado de estos en base de datos ejemplos: texto, numeros enteros, numeros decimales, boleanos, llaves foraneas a partir de algun catalogo precargado de opciones...etc.
-[ ] Borrado de registros de forma logica, por medio de estatus
-[ ] Login, logout, recuperar contraseñs, gestion de usuarios, registro...etc.
-[ ] Utilizar javascript o preferentemente jquery para la construccion de las vistas de listas y formularios.
-[ ] Utilizar alguna plantilla y estilos ccs
-[ ] Utilizar completamente protocolos html 5
-[ ] Validaciones de formularios segun el tipo de dato
-[ ] Probar en la mayoria de los navegadores que funcionen todas las pantallas
-[ ] Intentar implementar el formulario en forma pantalla y en forma de ventana de dialogo (modal)
-[ ] Integrar rest framework o otra libreria mas actualizada mas comodo
-[ ] Consultas ajax en formularios de seleccion de catalogos (ejemplo catalogo de estados) en campos Select o Input autocompletados (preferentemente)
-[ ] Subir archivos en formulario
-[ ] Mostrar descarga de archivos en pantalla de detalle de registro
-[ ] Empezar a leer sobre jasperserver, jasperreport, de momento