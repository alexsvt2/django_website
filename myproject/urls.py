from django.contrib import admin
from django.urls import path, include
from django.views.generic import TemplateView

# from rest_framework import routers
from apps.recursos_humanos import views
from django.conf import settings
from django.conf.urls.static import static

# router = routers.DefaultRouter()
# router.register('empleados', views.EmpleadoViewSet)
# router.register('departamentos', views.DepartamentoViewSet)

urlpatterns = [
    path('', TemplateView.as_view(template_name='myproject/home.html')),
    path('campo_pruebas/', TemplateView.as_view(template_name='myproject/campo_pruebas.html')),
    path('recursos_humanos/', include('apps.recursos_humanos.urls')),
    path('admin/', admin.site.urls),
    # path('api/', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('accounts/', include('django.contrib.auth.urls')),
    path('accounts/', include('apps.accounts.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)